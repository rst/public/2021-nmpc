"""
Creation of all plots that can be seen in the publication.

Please note, this scripts only visualises the result files. To create them
please run the actual simulation scripts first.

"""
import os

import numpy as np
import pyinduct as pi
from matplotlib import cm, pyplot as plt

from model_1d import Material, FEModel
from plotting import plot_helper as ph
from plotting.plot_helper import plot_delta
from stefan.test_model import res_file
from plotting.plot_settings import init_printing


figure_path = os.path.join(os.path.dirname(os.path.abspath(__file__)),
                           os.pardir,
                           "include")
data_path = os.path.join(os.path.dirname(os.path.abspath(__file__)),
                         os.pardir,
                         "data")


def plot_verification():
    """
    Plot for the model verification

    This routine uses the data generated in `test_model.py`. Run this script
    first if the result file is missing.
    """
    export_path = os.path.join(figure_path, "verification.pdf")
    plot_delta(res_file, export_path)


def plot_mpc():
    """
    Plots for the various mpc variants.

    This function assumes that `control.py` has been successfully run for the
    result files to be present.

    It will generate comparison plots for the inputs, objectives and
    temperatures.
    """
    sel_variants = [
        # "planning_K10.pkl",
        "ideal_case_K10_10.pkl",
        "ideal_case_K10_64.pkl",
        "ideal_case_K10_64_pu.pkl",
    ]
    data = ph.load_plot_data(sel_variants, use_relative_path=True)

    if 1:
        export_path = os.path.join(figure_path, "input_trajectories.pdf")
        ph.plot_inputs(data, export_path)

    if 1:
        export_path = os.path.join(figure_path, "objective_trajectories.pdf")
        ph.plot_objectives(data, export_path)

    if 1:
        export_path = os.path.join(figure_path, "temp_profile_{}.pdf")
        ph.plot_temperatures(data, export_path)


def plot_jacket_characteristics():
    material = Material(name="GaAs")
    z_end = 1
    n_fem = 3
    xs = np.ones(FEModel.get_x_dim(n_fem))
    us = np.ones(6)
    sys = FEModel(n_fem, z_end, material, xs, us)

    # build char polys
    def build_char_func(idx):
        b = sys.b_mantle[idx]
        c = sys.c_mantle[idx]
        d = sys.d_mantle[idx]

        def _char_expr(z):
            return b * z**2 + c * z + d

        return _char_expr

    funcs = [build_char_func(i) for i in range(3)]

    z_values = np.linspace(0, z_end, 1000)
    f_values = np.vstack([f(z_values) for f in funcs])

    fig, ax = plt.subplots()
    for idx in range(3):
        ax.plot(z_values, f_values[idx], label=f"{idx}")

    ax.legend()
    ax.grid()
    ax.set_xlabel(r"$z$ in \si{\metre}")
    ax.set_ylabel(r"$\psi_i(z)$")
    # plt.show()

    export_path = os.path.join(figure_path, "jacket_characteristics.pdf")
    fig.tight_layout(pad=0)
    fig.savefig(export_path)


def plot_vortex_characteristics():
    R = 1
    z_end = 1

    # build char polys
    def chi_r(z):
        return (2 * z - z_end) / (2 * z_end**2)

    def chi_z(r):
        return (R - 2 * r) / (2 * R**2)

    def chi(r, z):
        return chi_r(z), 2 * chi_z(r)

    n_points = 10
    r_values = np.linspace(0, R, n_points)
    z_values = np.linspace(0, z_end, n_points)

    chi_r_values = chi_r(z_values)
    chi_z_values = chi_z(r_values)

    RR, ZZ = np.meshgrid(r_values, z_values)
    chi_values = np.array(chi(RR, ZZ))
    abs_values = np.linalg.norm(chi_values, axis=0)

    fig, ax = plt.subplots()
    # ax.quiver(r_values, z_values, np.zeros_like(chi_values[0]), chi_values[1])
    q = ax.quiver(r_values, z_values, chi_values[0], chi_values[1],
                  abs_values,
                  # cmap="viridis"
                  )
    # ax.streamplot(r_values, z_values, chi_values[0], chi_values[1])
    cmap = cm.ScalarMappable(norm=q.norm, cmap=q.cmap)
    ac = fig.colorbar(cmap, ax=ax)
    ac.set_label(r"$\sqrt{\chi_{\mathrm{r}}^2(r,z) + \chi_{\mathrm{v}}^2 (r,z)}$")

    # ax.legend()
    ax.grid()
    ax.set_xlabel(r"$r$ in \si{\metre}")
    ax.set_ylabel(r"$z$ in \si{\metre}")
    # plt.show()

    export_path = os.path.join(figure_path, "vortex_characteristics.pdf")
    fig.tight_layout(pad=0)
    fig.savefig(export_path)


def plot_shape_functions():
    bounds = 0, 1
    nodes = pi.Domain(bounds, num=5)
    base = pi.LagrangeFirstOrder.cure_interval(nodes)

    z_values = np.linspace(*bounds, num=1000)

    fig, ax = plt.subplots()
    for i, func in enumerate(base):
        ax.plot(z_values, func(z_values), label=f"{i}")

    ax.legend()
    ax.set_xlabel(r"$z$ in \si{\metre}")
    ax.set_ylabel(r"$\varphi^N_i(z)$")
    # plt.show()
    export_path = os.path.join(figure_path, "shape_functions.pdf")
    fig.tight_layout(pad=0)
    fig.savefig(export_path)



if __name__ == '__main__':
    if 0:
        init_printing(mode="print")
        plot_verification()
        plot_mpc()

    else:
        init_printing(mode="talk")
        # plot_jacket_characteristics()
        # plot_vortex_characteristics()
        plot_shape_functions()

