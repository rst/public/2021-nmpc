"""
Evaluation routines for the simulation results.

WIP
"""
import casadi as ca


def eval_cost(self, x=None, u=None):
    if 1:
        self.get_boundaries_list(list_id=1)
    cost_per_term = {}
    if x is None and u is None:
        x = self.sol['x_sim']
        u = self.sol['u_sim']

    if self.number_of_inputs == 2:
        u_sp = self.u_sp1
    if self.number_of_inputs == 5:
        u_sp = self.u_sp2
    if self.number_of_inputs == 6:
        u_sp = self.u_sp3

    int_tol = 1e-14
    ode = {'x': self.x_sim,
           'p': ca.vcat((self.u,
                         self.x_sp, u_sp)),
           'ode': self.x_dot_sim, 'quad': self.cost_functions['L_gamma']}
    opts = {'tf': self.t_grid[1] - self.t_grid[0],
            'abstol': int_tol,
            'reltol': int_tol}

    for cost_term_name in self.cost_functions:
        # cost_term_name = 'L_dTdz'
        L_sim = []
        ode['quad'] = self.cost_functions[cost_term_name]
        simulator = ca.integrator('simulator', 'cvodes', ode, opts)

        # interpoliere
        # u_interp = sci.interpolate.interp1d(self.tt,u,kind='zero', axis=0)
        # print(u_interp(1))
        for n in range(0, x.shape[0]):
            p = ca.vcat((u[n, :],
                         self.x_ref_sim[0],
                         self.u_ref[0]))
            sim_n = simulator(x0=x[n, :], p=p)
            L_sim.append(sim_n['qf'])
        cost_per_term[cost_term_name] = L_sim

        # test_sum = sum(L_sim) #+ sum(L_dTdz)  # + sum(L_dTdz)
        #    print(test_sum)
    '''
    for cost_term in cost_terms:
        cost = []

        f = ca.Function('f', [self.x_sim, self.x_sp], [cost_term])
        lambdified_f = lambda x : f(x,self.x_ref_sim )
        print(lambdified_f(x_data[1, :]))
        for n in range(0, self.tt.size-1):
            l_sim_own = lambdified_f(x_data[n, :])
        for n in range(0, self.tt.size-1):
            integral = sci.integrate.quad(lambdified_f,n*deltaT,(n+1)*deltaT)
            cost.append(f(x_data[n, :], self.x_ref_sim)*deltaT)
        plt.plot(self.tt[1:],cost)
        plt.plot(self.tt[1:],self.sol['L_sim'])
        plt.show()
        print('cost', cost)
    '''
    return cost_per_term
