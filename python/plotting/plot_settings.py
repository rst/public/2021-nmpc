import os
import numpy as np
from cycler import cycler
import matplotlib as mpl
from matplotlib.cm import get_cmap

# since matplotlib uses the arcane imperial system
CM_PER_INCH = 2.54

# an extract from tu TU-Dresden Corporate Design colors
HKS44K100 = '#0059a3'  # dunkelblau
HKS96K100 = '#727879'  # grau
HKS41K100 = '#0b2a51'  # hellblau
HKS36K100 = '#512947'  # braun
HKS33K100 = '#811a78'  # lila
HKS57K100 = '#007a47'  # grün
HKS65K100 = '#22ad36'  # hellgrün
HKS07K100 = '#e87b14'  # orange

c_cycle = cycler('color', [
    HKS44K100,
    HKS57K100,
    HKS33K100,
    HKS07K100,
    HKS65K100,
])
line_cycle = cycler("lw", ["--", "-"])


def init_printing(mode):
    """
    Initialize matplotlib settings.

    This includes tex, fonts and line widths. Font sizes will be different for
    print or beamer media.

    Args:
        mode (str): Either `print` or `talk`

    Returns:

    """
    mpl.rcParams['text.usetex'] = True
    # mpl.rcParams['text.latex.unicode'] = True

    if mode == "print":
        mpl.rcParams['font.size'] = 17
        # mpl.rcParams['axes.labelsize'] = 15
        # mpl.rcParams['xtick.labelsize'] = 15
        # mpl.rcParams['ytick.labelsize'] = 15
        mpl.rcParams['lines.linewidth'] = 2
    elif mode == "talk":
        mpl.rcParams['font.size'] = 25
        mpl.rcParams['axes.labelsize'] = 30
        mpl.rcParams['lines.linewidth'] = 2.0
    else:
        raise NotImplementedError

    extra_pckgs = "\n".join([
        r'\usepackage{amsmath}',
        r'\usepackage{amssymb}',
        r'\usepackage{mathtools}',
        r'\usepackage[scientific-notation=true]{siunitx}',
    ])
    mpl.rcParams['text.latex.preamble'] += extra_pckgs
    mpl.rcParams['pgf.preamble'] = extra_pckgs

    # import macros
    # macro_line = r'\input{' + os.getcwd() + '/../latex/makros.tex}'
    # mpl.rcParams['text.latex.preamble'] += macro_line
    # mpl.rcParams['pgf.preamble'] += macro_line

    # set figure size
    golden_ratio = (1 + 5 ** 0.5) / 2
    width = 20  # [cm]
    height = width / golden_ratio
    mpl.rcParams['figure.figsize'] = (width/CM_PER_INCH, height/CM_PER_INCH)
    mpl.rcParams['savefig.dpi'] = 300

def get_colorcycle(len):
    """
    sets a global color cycle that fits color map
    :param len: how many elements should th cycle have
    :return:
    """
    cmap_name = "viridis"
    # cmap_name = "viridis_r"
    cmap = get_cmap(cmap_name)
    # mpl.rcParams['axes.prop_cycle'] = [cmap(k) for k in np.linspace(0, 1, len, endpoint=True)]
    return [cmap(k) for k in np.linspace(0, 1, len, endpoint=True)]
