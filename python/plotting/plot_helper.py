from typing import Union
import logging
import os
import os.path
import pickle

import numpy as np
import matplotlib.pyplot as plt
from pyinduct import EvalData
from matplotlib import cm, pyplot as plt

import plotting.plot_settings as ps

tm = 1511.15

logger = logging.getLogger(__name__)
p_keys = [
    "t_grid", "t_step",
    "x_sim", "u_sim",
    "x_opt", "u_opt",
    "L_opt", "J_opt",
    "alpha_a", "alpha_b",
    "temp", "grad_s", "grad_l",
    "gamma", "gamma_dt",
]
ls = [None] * 10
labels = ["A", "B", "C", "D", "E", "F", "G"]
min_ls = "--"
max_ls = ":"


def colourplot(temp_data: Union[list[np.ndarray, np.ndarray, np.ndarray], EvalData],
               gamma_data: np.ndarray,
               temp_ref: tuple[float, float] = None,
               gamma_ref: float = None,
               show_plot: bool = False, path: str = None):
    """
    Plot the spatial and temporal temperature distribution.

    Args:
        temp_data: Temperature data, either 3 arrays with t, z, an temp data or
            Eval data object.
        gamma_data: Interface positions.
        temp_ref: Reference temperature extrema to use for color bar.
        gamma_ref: Reference interface, defaults to None.
        show_plot: If True, show the result.
        path: If given, store the result under the given name.

    """
    # collect data
    if isinstance(temp_data, list):
        tt = temp_data[0]
        zz = temp_data[1]
        temperature = temp_data[2]
    elif isinstance(temp_data, EvalData):
        tt = temp_data.input_data[0].points
        zz = temp_data.input_data[1].points
        temperature = temp_data.output_data
    else:
        raise NotImplementedError

    # reduce pdf size
    step = 10
    tt = tt[::step]
    gamma_data = gamma_data[::step]
    temperature = temperature[::step]

    # mask areas from the crystal with temp higher than tm
    mask = temperature > tm
    for n in range(len(gamma_data)):
        melt_indexes = np.where(zz > gamma_data[n])
        for index in melt_indexes:
            mask[n, index] = False
    temperature = np.ma.array(temperature, mask=mask)

    tt = tt / 3600  # s -> h
    zz = zz * 1000  # m -> mm
    ZZ, TT = np.meshgrid(zz, tt)

    # plot temps
    fig, ax = plt.subplots()
    ax.set_xlabel(r"$t$ in \si{\hour}")
    ax.set_ylabel(r"$z$ in \si{\milli\metre}")

    if temp_ref is not None:
        c_levels = np.hstack((
            np.linspace(temp_ref[0], tm, 10)[:-1],
            np.linspace(tm, temp_ref[1], 10)
        ))
        c_levels = np.round(c_levels)
    else:
        c_levels = 20

    # get colors
    cmap = cm.get_cmap("inferno")
    ax.patch.set_color('grey')  # '.25
    ax.patch.set_hatch('+')

    # fix invalid edge pixels by repainting them
    for i in range(3):
        cf = ax.contourf(TT, ZZ, temperature,
                         cmap=cmap,
                         # colors=colors,
                         levels=c_levels,
                         linestyles=None,
                         antialiased=True,
                         # Nchunk=0,
                         # linewidths=0,
                         # lw=0,
                         # ls=None,
                         )

    bar = fig.colorbar(cf, ax=ax)
    bar.set_label(label=r"$T(z,t)$ in \si{\kelvin}")

    # plot gamma
    ax.plot(tt, gamma_data * 1e3, c=ps.HKS41K100,
            label=r"$\gamma(t)$")
    if gamma_ref is not None:
        gamma_ref *= 1000
        ax.axhline(gamma_ref, ls='--', c=ps.HKS57K100,
                   label=r"$\gamma_{\mathrm{ref}}$")
    ax.legend()

    fig.tight_layout()
    if path is not None:
        fig.savefig(path)
    if show_plot:
        plt.show()
    return fig


def plot_delta(f_name: str, path: str = None):
    """
    Plot the deviations between the reference and actual trajectories for gamma

    Args:
        f_name: Name of the result file.
        path: If not None, store the plot under this path.

    """
    with open(f_name, "rb") as f:
        data = pickle.load(f)

    params, gamma_res, gamma_err, temp_res, temp_err = data
    tt = params["t_values_sim"]
    gamma_data_analytic = params["gamma_ana"]

    fig, ax = plt.subplots()
    ax.set_xlabel(r"$t$ in \si{\hour}")
    ax.set_ylabel(r"$\Delta \gamma(t)$ in \si{\milli\metre}")

    for order in sorted(gamma_res.keys()):
        delta_gamma = gamma_res[order] - gamma_data_analytic
        ax.plot(tt / 3600, delta_gamma * 1e3, label=order)

    ax.legend(loc="best", fancybox=True, title="N")  # , prop={'size': 9})
    ax.grid()
    fig.tight_layout()
    if path is not None:
        fig.savefig(path)
    # plt.show()


def plot_inputs(data: dict, export_path: str):
    """
    Plot the input trajectories.

    Args:
        data: Dict holding the result data.
        export_path: Path to store the result plot at.
    """
    t_grid = data["t_grid"][0] / 3600
    u_data = np.array(data["u_sim"])
    n_u = u_data.shape[2]

    if 0:
        f, ax = plt.subplots()
        ax.plot(t_grid, u_data[0, :, -1])
        # ax.plot(t_grid, u_data[1, :, -1])
        plt.show()
        quit()

    f, axs = plt.subplots(int(n_u / 2 + .5),
                          2,
                          sharex=True,
                          )
    for idx, ax in enumerate(axs.flatten()):
        u_sim = u_data[:, :, idx]

        # Labels
        if idx == 0:
            ax.set_ylabel(r"$u_s(t)$ in $\tfrac{kW}{m^2}$")
        if idx == 1:
            ax.set_ylabel(r"$u_l(t)$ in $\tfrac{kW}{m^2}$")
        if idx == 2:
            ax.set_ylabel(r"$\bar{u}_{m,1}(t)$ in $\tfrac{kW}{m^2}$")
        if idx == 3:
            ax.set_ylabel(r"$\bar{u}_{m,2}(t)$ in $\tfrac{kW}{m^2}$")
        if idx == 4:
            ax.set_xlabel(r"$t$ in h")
            ax.set_ylabel(r"$\bar{u}_{m,3}(t)$ in $\tfrac{kW}{m^2}$")
        if idx == 5:
            ax.set_xlabel(r"$t$ in \si{\hour}")
            ax.set_ylabel(r"$\bar{u}_v(t)$ in $\tfrac{mm}{h}$")

        # Limits
        if idx < 2:
            # top and bottom heater in kW
            ax.axhline(-15, ls=min_ls, color=ps.HKS41K100)  #, label="min")
            ax.axhline(+15, ls=max_ls, color=ps.HKS41K100)  #, label="max")
        elif idx < 5:
            # jacket heaters in kW
            ax.axhline(-1.5, ls=min_ls, color=ps.HKS41K100)
            ax.axhline(+1.5, ls=max_ls, color=ps.HKS41K100)
        else:
            # melt convection in mm/h
            ax.axhline(-10, ls=min_ls, color=ps.HKS41K100, label="min")
            ax.axhline(10, ls=max_ls, color=ps.HKS41K100, label="max")

        # the actual plots
        for k, u in enumerate(u_sim):
            if idx < 5:
                ax.plot(t_grid, u / 1000, ls=ls[k],
                        ds='steps')  # , label=labels[k])  # W/m^2 -> kW/m^2
            else:
                ax.plot(t_grid, u * 1000 * 3600, ls=ls[k],
                        ds='steps', label=labels[k])  # m/s -> mm/h

        ax.grid(True)

    axs[2, 0].xaxis.set_ticks(list(range(0, 71, 10)))
    axs[2, 1].xaxis.set_ticks(list(range(0, 71, 10)))

    f.tight_layout()
    if 0:
        axs[0, 0].legend(
            bbox_to_anchor=(0.2, 1.4, 2, .01),
            loc='center',
            ncol=2 + u_data.shape[2],
            mode="expand",
            borderaxespad=0.)
    else:
        f.legend(bbox_to_anchor=(0.14, .9, .83, .1),
                 loc='center',
                 ncol=2 + u_data.shape[2],
                 mode="expand",
                 borderaxespad=0.)
    f.subplots_adjust(top=0.88)
    f.savefig(export_path)


def plot_objectives(data: dict, export_path: str):
    """
    Plot the trajectories of the custom objectives.

    Args:
        data: Dict holding the result data.
        export_path: Path to store the result plot at.
    """
    t_grid = data["t_grid"][0] / 3600
    gamma = data["gamma"]
    gamma_dt = np.array(data["gamma_dt"]) * 1000 * 3600
    grad_s = np.array(data["grad_s"])
    grad_l = np.array(data["grad_l"])

    orig_size = plt.rcParams['figure.figsize']
    this_size = orig_size
    fig, ax = plt.subplots(3, sharex=True, figsize=this_size)

    # gamma dot
    ax[0].axhline(0, ls=min_ls, color=ps.HKS41K100, label="min")
    ax[0].axhline(7, ls=max_ls, color=ps.HKS41K100, label="max")
    ax[0].set_ylabel(r"$\dot{\gamma}(t)$ in $\tfrac{mm}{h}$")
    for idx, g in enumerate(gamma_dt):
        ax[0].plot(t_grid, g, label=labels[idx])
    ax[0].grid()

    # liquid gradient
    ax[1].axhline(20, ls=min_ls, color=ps.HKS41K100)
    ax[1].set_ylabel(r"$\partial_z T_l(\gamma(t), t)$ in $\tfrac{K}{m}$",
                     labelpad=20)
    for idx, g in enumerate(grad_l):
        ax[1].plot(t_grid, g)
    ax[1].grid()

    # v over g
    # ax[2].axhline(0, ls=min_ls, color=ps.HKS41K100)
    ax[2].axhline(7, ls=max_ls, color=ps.HKS41K100)
    ax[2].set_ylabel(r"$\eta(t)$ in $\tfrac{(mm)^2}{Kh}$")
    for idx, g in enumerate(grad_s):
        ax[2].plot(t_grid, gamma_dt[idx] / (g * 1e-3), label=labels[idx])
    ax[2].grid()

    ax[-1].set_xlabel(r"$t$ in \si{\hour}")

    ax[0].legend(bbox_to_anchor=(0., 1.15, 1., .1),
                 loc='lower left',
                 ncol=2 + grad_s.shape[0],
                 mode="expand",
                 borderaxespad=0.)

    fig.tight_layout()
    fig.savefig(export_path)
    # plt.show()
    return


def load_plot_data(file_names: list[str], use_relative_path: bool = False
                   ) -> dict:
    """
    Helper function to load a set of results for comparison.

    Args:
        file_names: List of file names to load.
        use_relative_path: If True, the default data path relative to the
            current directory is used.

    Returns: Dict lists for every key holding the results.

    """
    data_path = os.path.join(os.path.dirname(os.path.abspath(__file__)),
                             os.pardir,
                             os.pardir,
                             "data")
    res = {k: [] for k in p_keys}
    for f_name in file_names:
        if use_relative_path:
            f_name = os.path.join(data_path, f_name)
        if not os.path.isfile(f_name):
            logger.warning(f"File {f_name} not found")
            continue

        with open(f_name, "rb") as f:
            data = pickle.load(f)

        for k in p_keys:
            res[k].append(data[k])

    return res


def plot_costs_1():
    """ WIP """
    raise NotImplementedError
    if plot_cost:
        plt.subplot(2, 2, 3)
    else:
        plt.subplot(3, 1, 3)
    plt.ylabel(
        r'$v/G \cdot 10^{-3}$ in $\frac{m^2}{K \cdot h}$', fontsize=15)

    plt.xlabel(r'$t$ in $h$', fontsize=15)
    for idx, dTdz_l in enumerate(dTdz_l_data):
        # plt.plot(tt, dTdz_l)
        plt.plot(tt, gamma_dot_data[idx] / dTdz_l, ls=ls[idx],
                 label=labels[idx])
    plt.plot(tt, np.full(shape, 0.2), c='red')
    plt.legend(loc="upper right")
    plt.rc('text', usetex=True)
    plt.grid(True)

    if plot_cost:
        plt.subplot(2, 2, 4)

        plt.ylabel(
            r'$l(x(t),x_{\mathit{ref}}(t)) $', fontsize=15)
        plt.xlabel(r'$t$ in $h$', fontsize=15)
        ls = []
        y = []
        cost_dict.pop('L_dTdz')
        cost_dict.pop('L_dTdz_gamma')
        cost_dict.pop('L_v_over_g')
        for cost_term_name in cost_dict:
            if cost_term_name == 'L_gamma':
                ls.append(r'$l_{\gamma}(t)$')
            if cost_term_name == 'L_dTdz':
                ls.append(r'$l_{\partial_z T_l(z)}(t)$')
            if cost_term_name == 'L_dTdz_gamma':
                ls.append(r'$l_{\partial_z T_l(\gamma)}(t)$')
            if cost_term_name == 'L_gamma_dot':
                ls.append(r'$l_{\dot{\gamma}}(t)$')
            if cost_term_name == 'L_v_over_g':
                ls.append(r'$l_{v/G}(t)$')
            y.append(cost_dict[cost_term_name])

        plt.stackplot(self.t_grid / 3600, y, labels=ls)

        plt.legend()

    plt.rc('text', usetex=True)
    plt.grid(True)
    # plt.show()

    if file_name is not None:
        plt.tight_layout()
        plt.savefig("results/pics/" + file_name + '_growth.pgf', format='pgf')
    plt.show()


def plot_costs_2():
    """ WIP """
    plt.ylabel(
        r'$l(x(t),x_{\mathit{ref}}(t)) $', fontsize=15)
    plt.xlabel(r'$t$ in $h$', fontsize=15)
    ls = []
    y = []
    cost_dict.pop('L_dTdz')
    cost_dict.pop('L_dTdz_gamma')
    cost_dict.pop('L_v_over_g')
    for cost_term_name in cost_dict:
        if cost_term_name == 'L_gamma':
            ls.append(r'$l_{\gamma}(t)$')
        if cost_term_name == 'L_dTdz':
            ls.append(r'$l_{\partial_z T_l(z)}(t)$')
        if cost_term_name == 'L_dTdz_gamma':
            ls.append(r'$l_{\partial_z T_l(\gamma)}(t)$')
        if cost_term_name == 'L_gamma_dot':
            ls.append(r'$l_{\dot{\gamma}}(t)$')
        if cost_term_name == 'L_v_over_g':
            ls.append(r'$l_{v/G}(t)$')
        y.append(cost_dict[cost_term_name])
    # plt.plot(tt[1:], cost_dict['L_gamma_dot'], label=r'$l_{v/G}(t)$')
    # plt.show()

    plt.stackplot(self.t_grid / 3600, y, labels=ls)
    # plt.plot(tt, cost_term_name['L_gamma_dot'], label=r'$l_{\dot{\gamma}}(t)$')
    # plt.plot(tt, cost_term_name['L_dTdz_gamma'],label=r'$l_{\delta_z T_l(\gamma)}(t)$')

    # plt.axvline(35, ls='--', color='black')
    plt.legend()


def plot_temperatures(data, export_path):
    """
    Plot the temperature distributions for all given runs

    The first run will be used as a reference to create identical color bars
    for each plot.

    Args:
        data: Dict holding the result data.
        export_path: Path to store the result plot at.
    """
    all_temps = np.stack([t.output_data for t in data["temp"]])
    ref_temps = np.floor(all_temps.min()), np.ceil(all_temps.max())
    if 0:
        ref_temps = None
    for idx in range(len(data["temp"])):
        temp_data = data["temp"][idx]
        gamma_data = data["gamma"][idx]
        colourplot(temp_data, gamma_data,
                   temp_ref=ref_temps,
                   gamma_ref=0.38,
                   path=export_path.format(idx))
