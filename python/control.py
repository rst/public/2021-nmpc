"""
contains all scripts to compute and plot mpc process

"""
import logging
import numpy as np

from mpctoolbox import MPCMode, OCPMode
from vgf import VGFProcess


def run_scenario(file_name,
                 mode=MPCMode.UNLIMITED,
                 n_fem_sim=None,
                 initial_error=False,
                 param_uncertainties=False,
                 heater_issues=False,
                 heater_defect=False,
                 ):
    tester = VGFProcess(ocp_mode=OCPMode.FREE,
                        dom_len=0.4,
                        n_fem=10,
                        number_of_inputs=6,
                        mpc_mode=mode,
                        N=20,
                        t_step=30 * 60,  # [] = s
                        t_end=70 * 60 * 60,  # [] = s
                        n_fem_sim=n_fem_sim,
                        initial_error=initial_error,
                        param_deviation=param_uncertainties,
                        heater_issues=heater_issues,
                        heater_defect=heater_defect)
    tester.simulate()
    tester.post_process()
    tester.save_data(file_name)


if __name__ == "__main__":
    # configure loggers
    root_logger = logging.getLogger()
    formatter = logging.Formatter("%(asctime)s "
                                  "- %(name)s "
                                  "- %(levelname)s "
                                  "- %(message)s")

    # info on the commandline
    cmd_handler = logging.StreamHandler()
    cmd_handler.setLevel(logging.INFO)
    cmd_handler.setFormatter(formatter)
    logging.getLogger().addHandler(cmd_handler)

    # debug message to file
    dbg_handler = logging.FileHandler("debug.log")
    dbg_handler.setLevel(logging.DEBUG)
    dbg_handler.setFormatter(formatter)
    logging.getLogger().addHandler(dbg_handler)

    # configure the output of toolbox modules
    l_mpc = logging.getLogger("mpctoolbox.core.mpc")
    l_mpc.setLevel(logging.DEBUG)
    l_sim = logging.getLogger("mpctoolbox.core.simulation")
    l_sim.setLevel(logging.INFO)

    # configure output of vgf module
    logging.getLogger("vgf").setLevel(logging.INFO)

    # set random seed
    np.random.seed(20212201)

    if 1:
        # check if the overall setup is feasible by performing a planning
        run_scenario(file_name="planning_K10",
                     mode=MPCMode.OPEN_LOOP,
                     n_fem_sim=10,
                     )
    if 1:
        # control setup where the ref model matches the sim model
        run_scenario(file_name="ideal_case_K10_10",
                     n_fem_sim=10,
                     )
    if 1:
        # control setup with N_sim != N_ref
        run_scenario(file_name="ideal_case_K10_64",
                     n_fem_sim=64,
                     )
    if 1:
        # setup where N_sim != N_ref and with parameter uncertainties
        run_scenario(file_name="ideal_case_K10_64_pu",
                     n_fem_sim=64,
                     param_uncertainties=True
                     )

    logging.shutdown()
