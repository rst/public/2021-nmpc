"""
Verification of the FEM approximation against the analytic solution

This is done for different approximation orders.
"""
import logging
import os
import pickle
import numpy as np
from matplotlib import pyplot as plt
import pyinduct as pi

from model_1d import Material, FEModel
import analytic_solution as ana
from plotting.plot_helper import colourplot, plot_delta

logger = logging.getLogger(__name__)
res_path = os.path.join(os.path.dirname(os.path.abspath(__file__)),
                        os.pardir,
                        os.pardir,
                        "data")
ana_file = os.path.join(res_path, "analytic_2phase.pkl")
res_file = os.path.join(res_path, "verification_2phase.pkl")


def build_ana_sol(one_phase, show_plot=False):
    # solution domains
    t_values = np.linspace(0, 10 * 3600, int(1e4))
    t_start_sim = 1 * 3600
    sim_sel = t_values > t_start_sim
    t_values_sim = t_values[sim_sel]
    params = dict(
        t_values_sim=t_values_sim,
    )

    # args for analytic solution
    mat = Material(name="GaAs_const_rho", uncertainty_factor=1)
    ana_kwargs = dict(
        t_s=mat.T_m - 100,
        t_l=mat.T_m if one_phase else mat.T_m + 100,
        material=mat,
        dom_len=1,
        t_values=t_values,
    )

    # eval analytic solution
    gamma_data_analytic, temp_data_analytic, lam = ana.ground_truth(
        **ana_kwargs,
        show_plot=show_plot,
    )
    ana_kwargs["lam"] = lam
    params["ana_args"] = ana_kwargs
    params["gamma_ana"] = gamma_data_analytic[sim_sel]
    params["temp_ana"] = temp_data_analytic([t_values_sim])

    # derive system inputs for FE simulation. Only bottom and top heater != 0
    u_u, u_o = ana.get_neumann_boundaries(**ana_kwargs)
    u_m_0 = u_m_1 = u_m_2 = v_z = np.full_like(u_u, 0)
    u_data = np.array([u_u, u_o, u_m_0, u_m_1, u_m_2, v_z])
    params["u"] = u_data[:, sim_sel]

    return params


def test_approximation(one_phase=False, show_plot=False):
    try:
        with open(ana_file, "rb") as f:
            params = pickle.load(f)
            logger.warning("Loaded precomputed analytic solution!")
    except FileNotFoundError:
        logger.info("Loaded precomputed analytic solution!")
        params = build_ana_sol(one_phase)
        with open(ana_file, "wb") as f:
            pickle.dump(params, f)

    gamma_res = {}
    gamma_err = {}
    temp_res = {}
    temp_err = {}
    for n_fem in [4, 8, 16, 32, 64]:
        print("Computing order {}".format(n_fem))
        params["n_fem"] = n_fem
        temp, gamma = eval_approx(params, show_plot)
        gamma_res[n_fem] = gamma
        temp_res[n_fem] = temp

        g_err = gamma - params["gamma_ana"]
        t_err = temp - params["temp_ana"]
        temp_err[n_fem] = t_err
        gamma_err[n_fem] = g_err
        print("Error in gamma: {}".format(np.sum(g_err**2)))

        if show_plot:
            f_ref = colourplot(params["temp_ana"], params["gamma_ana"])
            f_sim = colourplot(temp, gamma)
            f_a = pi.PgAnimatedPlot(temp, replay_gain=1e4)
            pi.show(show_mpl=True)

    res = [params, gamma_res, gamma_err, temp_res, temp_err]
    print("Dumping results to {}".format(res_file))
    with open(res_file, "wb") as f:
        pickle.dump(res, f)


def eval_approx(params, show_plot=False):
    xs = np.ones(FEModel.get_x_dim(params["n_fem"]))
    xs[-1] = 1e3
    us = np.ones(6)
    us[:2] = 1e-4
    sys = FEModel(params["n_fem"],
                  params["ana_args"]["dom_len"],
                  params["ana_args"]["material"],
                  xs, us,
                  )

    # initial state
    gamma_0 = params["gamma_ana"][0]
    temp_0 = params["temp_ana"]

    def ev_wrapper(z):
        return temp_0([[0], [z]], as_eval_data=False).squeeze()

    x_0 = sys.get_state(ev_wrapper, gamma_0)
    u = params["u"]

    # run simulation
    t_sim = params["t_values_sim"]
    temp_data, gamma_data = sys.simulate(t_sim,
                                         x_0,
                                         u,
                                         show_pg=show_plot,
                                         num_z=1000)
    return temp_data, gamma_data


if __name__ == '__main__':
    logging.basicConfig()
    l_model = logging.getLogger("model_1d")
    l_model.setLevel(logging.DEBUG)
    test_approximation(show_plot=False)
