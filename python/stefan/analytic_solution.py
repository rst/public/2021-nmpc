"""
Functions to create and evaluate analytic solutions of a 1d Stefan Problem
"""
import logging
import numpy as np
from scipy.optimize import fsolve
import scipy.special as scisp
import pyinduct.core as cr

from model_1d import Material
from plot_helper import colourplot

logger = logging.getLogger(__name__)


def get_analytic_gradients(
        t_s: float, t_l: float, material: Material, lamda: float,
        dom_len: float, t_values: np.ndarray
) -> tuple[np.ndarray, np.ndarray, np.ndarray, np.ndarray]:
    """
    Compute the spatial derivatives of the analytic solution.

    Args:
        t_s: Initial temperature in the solid part.
        t_l: Initial temperature in the liquid part.
        material: Material to use.
        lamda: Value of the coefficient lambda.
        dom_len: Length of the problem domain,
        t_values: Time steps at which the solution is desired.

    Returns:

    """
    alpha = material.alpha
    t_m = material.T_m
    if t_values[0] == 0:
        t_values[0] = t_values[1] / 3
    t_dz_u = (t_m - t_s) / (
                np.sqrt(alpha[0] * np.pi * t_values) * scisp.erf(lamda))

    exponents = -(dom_len / (2 * np.sqrt(alpha[1] * t_values))) ** 2
    exponents[exponents < -1e2] = -1e2

    t_dz_o = (t_l - t_m) * np.exp(exponents) / \
             (np.sqrt(alpha[1] * np.pi * t_values) *
              scisp.erfc(lamda * (np.sqrt(alpha[0] / alpha[1]))))

    gamma = 2 * lamda * np.sqrt(alpha[0] * t_values)
    t_dz_s_gamma = (t_m - t_s) * np.exp(
        -(gamma / (2 * np.sqrt(alpha[0] * t_values))) ** 2) / (
                           np.sqrt(alpha[0] * np.pi * t_values) * scisp.erf(
                       lamda))
    t_dz_l_gamma = (t_l - t_m) * np.exp(
        -(gamma / (2 * np.sqrt(alpha[1] * t_values))) ** 2) / \
                   (np.sqrt(alpha[1] * np.pi * t_values) *
                    scisp.erfc(lamda * (np.sqrt(alpha[0] / alpha[1]))))

    return t_dz_u, t_dz_o, t_dz_s_gamma, t_dz_l_gamma


def get_neumann_boundaries(t_s: float, t_l: float, material: Material,
                           dom_len: float, t_values: np.ndarray,
                           lam: float = None,
                           ) -> tuple[np.ndarray, np.ndarray]:
    """
    Computes the corresponding input trajectories to achieve the same analytical
    solution as the "Von-Neumann" solution. By taking the respective derivatives
    of the analytic expressions.

    Args:
        t_s: Initial temperature in the solid part.
        t_l: Initial temperature in the liquid part.
        material: Material to use.
        dom_len: Length of the problem domain,
        t_values: Time steps at which the solution is desired.
        lam: Value of the coefficient lambda.

    """
    c, alpha, k, lat_heat, t_m, rho_m, v = material.get_params()
    if lam is None:
        lam = compute_lambda(t_s, t_l, material)

    t_dz_u, t_dz_o, t_dz_s_gamma, t_dz_l_gamma = get_analytic_gradients(
        t_s, t_l, material, lam, dom_len, t_values)

    q_u = -k[0] * t_dz_u
    q_o = k[1] * t_dz_o

    return q_u, q_o


def transcendental_function(lamda: float, mat: Material, t_s, t_l) -> float:
    """ Implicit equation that is to be solved for lambda """
    sts = (mat.c[0] / mat.L) * (mat.T_m - t_s)
    stl = (mat.c[1] / mat.L) * (t_l - mat.T_m)
    denominator_lam = np.exp(lamda ** 2) * scisp.erf(lamda)
    denominator_v_lam = mat.v * np.exp((mat.v ** 2) * (lamda ** 2)) * scisp.erfc(
        mat.v * lamda)
    res = sts / denominator_lam - stl / denominator_v_lam - lamda * np.sqrt(np.pi)
    return res


def ground_truth(t_s: float, t_l: float,
                 material: Material, dom_len: float,
                 t_values: np.ndarray, show_plot: bool = False):
    """
    Computes trajectories for temperatures and gamma based on the "Von-Neumann"
    solution.

    Args:
        t_s: Initial temperature in the solid part.
        t_l: Initial temperature in the liquid part.
        material: Material to use.
        dom_len: Length of the problem domain,
        t_values: Time steps at which the solution is desired.
        show_plot: If True, show a plot with the results.

    Returns: Tuple of (gamma, temp, lamda) trajectories.

    """
    z_steps = 500
    t_steps = t_values.size
    c, alpha, k, lat_heat, t_m, rho_m, v = material.get_params()
    lam = compute_lambda(t_s, t_l, material)

    # compute gamma
    gamma = 2 * lam * np.sqrt(alpha[0] * t_values)

    if dom_len is None:
        zz = np.linspace(0, 2 * gamma[-1], num=z_steps)
    else:
        zz = np.linspace(0, dom_len, num=z_steps)
    t_end = np.zeros((z_steps, t_steps))
    t_end[:, 0] = np.full_like(t_end[:, 0], t_l)

    # compute Temps
    for i in range(t_steps):
        for j in range(z_steps):
            if zz[j] <= gamma[i]:
                erf_z_t = scisp.erf(
                    zz[j] / (2 * np.sqrt(alpha[0] * t_values[i])))
                if erf_z_t < 1e-100:
                    erf_z_t = 0
                t_end[j, i] = t_s - (t_s - t_m) * erf_z_t / scisp.erf(lam)
            else:
                erfc_z_t = scisp.erfc(
                    zz[j] / (2 * np.sqrt(alpha[1] * t_values[i])))
                if erfc_z_t < 1e-100:
                    erfc_z_t = 0
                t_end[j, i] = t_l + (t_m - t_l) * erfc_z_t / scisp.erfc(
                    lam * v)

    tt_dom = cr.Domain(points=t_values)
    zz_dom = cr.Domain(points=zz)
    temp_data = cr.EvalData([tt_dom, zz_dom], t_end.T)

    if show_plot:
        colourplot(temp_data, gamma, zz[-1], True)

    return gamma, temp_data, lam


def compute_lambda(t_s, t_l, material) -> float:
    """
    Compute the value of Lambda by finding the zeros of a transcendental 
    function.
    
    Args:
        t_s: Solid temperature.
        t_l: Liquid temperature.
        material: Material to use.

    Returns: Value of gamma.
    """
    lam, info, ier, msg = fsolve(transcendental_function,
                                 x0=0.1,
                                 args=(material, t_s, t_l),
                                 full_output=True)
    logger.info(f"lambda: {lam}")
    logger.info(info)
    logger.info(ier)
    logger.info(msg)
    return lam.squeeze()
