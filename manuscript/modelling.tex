\section{Infinite dimensional model}
\label{sec:modelling}

As the foundation for model based control, this section introduces a reduced
one dimensional distributed parameter model based on the real \gls{vgf}
process plant.

\subsection{Two-dimensional model}

In both crystal and melt, the quantity under consideration is given by the
%distribution of the system temperature
scalar temperature field $\tfreep$
at the position vector $\bs{p}$ and time $t$.
In contrast to the diffusive-only energy
transport in the solid crystal, the liquid melt also enables convective heat
transport.  
However, due to the small Prandtl numbers of the materials (e.g.~\gls{gaas}
with \SI{0.068}{})
that denote the ratio of convective to diffusive heat transport,
the natural convection can be neglected and only the forced
convection $\bs{v}(\bs{p}, t)$ through the application of external magnetic
fields is modelled.  
Now, by assuming piecewise constant physical properties for the
solid and the liquid phase with the density $\dn$,
the specific heat capacity $\stc$, and thermal conductivity $\hc$,
the governing equation for each phase reads:
\begin{equation}
	\dn\stc \partial_t\tfreep =
		\hc\Delta\tfreep
		- \stc\dn\bs{v}(\bs{p},t) \nabla\tfreep.
	\label{eq:lin_heat_pde}
\end{equation}
%\begin{multline}
	%\partiell{}{t}\Big(\dn(\tfree)\stc(\tfree) \tfreep \Big)=
		%\hc(\tfree)\Delta\tfreep
		%+ \hc'(\tfree)\left(\nabla\tfreep\right)^2\\
		%- \Big(
			%\stc(\tfree)\dn(\tfree)\\
			%+ \left(\stc'(\tfree)\dn(\tfree) 
				%+ \stc(\tfree)\dn'(\tfree)
			%\right)\tfreep 
		%\Big)\bs{v}(\bs{p},t) \nabla\tfreep
	%\label{eq:nonlin_heat_pde}
%\end{multline}
Herein, the partial derivative of $\tfreep$ w.r.t.~$\circ$ is given by
$\partial_\circ \tfreep$, just as the Laplacian and gradient of
$\tfreep$ are denoted as $\Delta\tfreep$ and $\nabla\tfreep$, respectively.
As the equations for crystal and melt are similar, in the following only
one generic expression is stated, which can be specified for the solid or liquid
part by using the indices ``s'' and ``l'', respectively.
For example, there is no convection in the crystal and thus, $\bs{v}_{\mathrm{s}}(\bs{p},t) = 0$.

\begin{figure}
	\centering
	%\framebox[\linewidth]{
	\resizebox{\linewidth}{!}{\input{../include/system_sketch}}
	%}
	\caption{Simplified plant model with cylindrical coordinates $\bs{p}=(r, \varphi, z)$
		and the scaled coordinate frame $\bar z$.}
	\label{fig:system}
\end{figure}

Due to the geometry of the crucible, \eqref{eq:lin_heat_pde} is localised
in cylindrical coordinates with radius $r$, angle $\varphi$ as well as height
$z$  as shown in Figure \ref{fig:system}.
Since the plant is rotationally symmetric, the dependency on $\varphi$ can be
dropped which reduces the problem to the $2$d temperature distributions in the
meridional $rz$-planes of the crystal and the melt, each reaching from its system
boundary $\zb$ to the moving interface $\zic$:
\begin{align}
	\partial_t\tfreec &=
	\hd\left(
		\tfrac{1}{r}\partial_r\left(r\partial_r\tfreec\right)
		+ \partial^2_z\tfreec
	\right)
	\nonumber\\ &\quad
	- \vr \partial_r\tfreec
	\label{eq:lin_heat_pde_2d}
	\\ &\quad
	- \vz \partial_z\tfreec
	\nonumber
\end{align}
for $z \in (\zb, \zic)$, $r \in \left(0, R\right)$ and $t \in \mathbb{R}^+$.
Herein, $\hd \coloneqq \hc /(\dn\stc)$ denotes the thermal diffusivity and 
$\vr$ and $\vz$ represent the remaining components of $\bs{v}(r,z,t)$.

Due to the ongoing phase transition, the temperature at the interface
$\tfree(r,\zic,t)$ is fixed at the melting point temperature $\tm$.
%due to the
%ongoing phase transition, the heat flow in this description is not continuous
%at the phase boundary due to the release of latent heat within the
%solidification process and the abrupt change of the physical parameters between
%crystal and melt. 
Furthermore, due to the rotational symmetry, the radial heat flow at $r=0$
is always zero.  Thus, by denoting the top/bottom and jacket
heat flows as $q(r,t)$ and $\qm$, the boundary
conditions of \eqref{eq:lin_heat_pde_2d} read:
\begin{subequations}
	\begin{align}
		\label{eq:lin_heat_2d_bc_bot}
		\hc\partial_z \tfree(r, \zb, t) &= \bfd q(r,t)\\
		\label{eq:lin_heat_2d_bc_jacket}
		\hc\partial_r \tfree(R, z, t) &= \qm\\
		\label{eq:lin_heat_2d_bc_inner}
		\partial_r \tfree(0, z, t) &= 0\\
		\label{eq:lin_heat_2d_bc_gamma}
		\tfree(r,\zic, t) &= \tm
		% 2 phase variant
		%\label{eq:nonlin_heat_bc_bot}
		%\hcs\partial_z T(r, \zbs, t) &= \qs\\
		%\label{eq:nonlin_heat_bc_jacket_s}
		%\hcs\partial_r T(R, z, t) &= \qm \;\text{for}\; \zbs < z < \zic\\
		%\label{eq:nonlin_heat_bc_inner}
		%\partial_r T(0, z, t) &= 0\\
		%\label{eq:nonlin_heat_bc_gamma}
		%T(r,\zic, t) &= \tm\\
		%\label{eq:nonlin_heat_bc_jacket_l}
		%\hcl\partial_r T(R, z, t) &= \qm \;\text{for}\; \zic < z < \zbl\\
		%\label{eq:nonlin_heat_bc_top}
		%\hcl\partial_z T(r, \zbs, t) &= \ql.%
	\end{align}%
	\label{eq:lin_heat_2d_bcs}%
\end{subequations}
with the orientation factor $\bfd$ taking the value of $-1$ in the crystal and
$1$ in the melt.
Herein, $\qm$ describes an arbitrary lateral heat flow profile, 
in practice however, only three distinct heaters exist. 
Therefore, $\qm$ is given by
\begin{equation}
	\qm = \sum_{i=1}^{3} \psi_i(z) \inpm{i} = \psiv \inpmv
	\label{eq:jacket_flow}
\end{equation}
with the intensity profiles $\psi_i(z) \coloneqq b_i z^2 + c_i z + d_i$,
deduced from the plant geometry and experiments and condensed in
$\psiv \coloneqq \begin{pmatrix}
	\psi_1(z) &\psi_2(z) &\psi_3(z)
\end{pmatrix}$.
Furthermore, the actual jacket heat flows are denoted by $\inpm{i}$
with their corresponding vector
$\inpmv^T \coloneqq \begin{pmatrix}
	\inpm{1} &\inpm{2} &\inpm{3}
\end{pmatrix}$.

\subsection{One-dimensional model}

For the control design it is assumed
%\footnote{Please note that this assumption does not hold in practice since no
%bended interfaces can be modeled this way.}
that the heat loss in radial direction can be neglected.
This enables averaging over the radius and reducing the spatial domain to a line
with the new temperature defined as:
\begin{equation}
	\torigz \coloneqq \frac{2}{R^2}\int_0^R \tfreec r \dop r \:.
	\label{eq:average_temp}
\end{equation}
Furthermore, assume that the radial component of the forced convection field
has zero mean such that $\int\nolimits_0^R\vr\dop r = 0$.
Lastly, assume that $\vz$ can be written
as $\vz = \chi_{\mathrm{v}}(r, z)\inpbc$ with a fixed vortex-characteristic
$\chi_{\mathrm{v}}(r, z)$
%by analogy with \eqref{eq:jacket_flow}
and scaling $\inpbc$,
realised by the heater-magnet-modules.
Thus, integration of \eqref{eq:lin_heat_pde_2d} over $r$ and scaling by
$R^{-2}$ yields the final \gls{fbp} 
\begin{subequations}
	\label{eq:lin_heat_1d_sys}
	\begin{align}
		\label{eq:lin_heat_1d_pde}
		\begin{split}
			\partial_t \torigz &= \hd\partial_z^2 \torigz
			-\psi_{\mathrm{v}}(z)\inpbc \partial_z \torigz 
			\\ &\quad
			+\psiv\inpbmv
		\end{split}
		\\
		\label{eq:lin_heat_1d_bc_0}
		\hc\partial_z \torig(\zb, t) &= \bfd\inp\\
		\label{eq:lin_heat_eq_s_bc2}
		\torig(\zi, t) &= \tm
	\end{align}
\end{subequations} 
%\begin{subequations}
	%\label{eq:lin_heat_eqs}
	%\begin{align}
		%\label{eq:lin_heat_eq_s_pde}
		%\partial_t \ts(z, t) &= \hds\partial_z^2 \ts(z,t)
		%+\kms\qm
		%\\
		%\label{eq:lin_heat_eq_s_bc1}
		%\hcs\partial_z \ts (\zbs, t) &= \bfds\inps\\
		%\label{eq:lin_heat_eq_s_bc2}
		%\ts(\zi, t) &= \tm \\
		%\label{eq:lin_heat_eq_l_pde}
		%\partial_t \tl(z, t) &= \hdl\partial_z^2 \tl(z,t)
		%-v_z(t) \partial_z \tl(z, t) 
		%\nonumber\\&\hphantom{=} 
		%+\kml\qm
		%\\
		%\label{eq:lin_heat_eq_l_bc1}
		%\hcl\partial_z \tl (\zbl, t) &= \bfdl\inpl\\
		%\label{eq:lin_heat_eq_l_bc2}
		%\tl(\zi, t) &= \tm
	%\end{align}
%\end{subequations} 
where $\psi_{\mathrm{v}}(z) \coloneqq \frac{1}{R}\int_0^R \chi_{\mathrm{v}}(r,z) \dop r$
and $\inp \coloneqq \frac{1}{R} \int_0^R q(r,t) \dop r$ 
represent the radially averaged vortex characteristic and heat flow, respectively.
Furthermore, $\inpbmv \coloneqq \inpmv\,2/(R\dn\stc)$ denotes the scaled
former boundary \eqref{eq:lin_heat_2d_bc_jacket}
that now acts as in-domain actuation.

Next, examining the energy balance at the reduced interface $\zi$ yields the
\acrlong{sc} \citep{Stefan1891}
\begin{equation}
	\dmelt L\vi = \hcs\partial_z\ts(\zi, t) - \hcl\partial_z\tl(\zi, t)
	\label{eq:stefan_cond}
\end{equation}
which describes the evolution of the phase boundary.
Herein, $\dmelt$ denotes the density of the melt at melting temperature and $L$
the specific latent heat.



%\subsection{State space formulation}

%Together, \eqref{eq:lin_heat_eqs} and \eqref{eq:stefan_cond} form the \gls{tpsp} whose state is 
%given by
%\begin{equation}
	%\bs{\chi}(\cdot, t) = \begin{pmatrix}
		%%\ts(\cdot, t) \\ \tl(\cdot, t) \\ \zi
	%%\end{pmatrix} \in X = 
		%%L_2(\doms) \times L_2(\doml)
		%\torig(\cdot, t) \\ \zi
	%\end{pmatrix} \in \mathcal{X} = 
		%L_2([\zbs, \zbl])
		%\times (\zbs, \zbl) \:,
	%\label{eq:state}
      %\end{equation}
      %where $L_2([\zbs, \zbl])$ denotes the space of real-valued square-integrable functions defined on $[\zbs, \zbl]$.
%Note that the PDE-ODE-PDE system defined by 
%\eqref{eq:lin_heat_eqs} and \eqref{eq:stefan_cond} is inherently nonlinear since the domains of
%\eqref{eq:lin_heat_eq_s_pde} and \eqref{eq:lin_heat_eq_l_pde} depend on the state variable $\zi$.

%As both \glspl{pde} in \eqref{eq:lin_heat_eqs} are similar except for their parameters
%and inputs, the following sections will merely work with one generic expression
%whose parameters may be substituted by either the ones from crystal or the melt.
%%If terms from two different phases are to appear in the same expression,
%%as in \eqref{eq:stefan_cond}, the indeces are given.


%%% Local Variables:
%%% mode: latex
%%% TeX-master: "root"
%%% End:
