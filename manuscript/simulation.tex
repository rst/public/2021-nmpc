\section{Finite dimensional approximation}
\label{sec:fem_model}

Although the spatially distributed model's equations are quite elegant in terms
of notation, to apply the standard methods for \gls{nmpc}, a spatially lumped
model is needed.  Therefore, this section introduces a lumped nonlinear
state space system of the form
\begin{equation}
	\xd = f(\x, \uo)%,\quad \sysout = h(\x)
	\label{eq:nonlin_ss}
\end{equation}
with the initial state $\bs{x}(0) = \bs{x}^0$
using the \gls{fem}.


\subsection{Series expansion}
To obtain a finite dimensional approximation, the temperature distribution
$T(z,t)$ in each of the two phases is approximated by a test solution of order
$N$
\begin{equation}
	\torig^N\!(z,t) \coloneqq \sum\limits_{i=1}^N \varphiz{i}\wi,
	\label{eq:gf_series_trunc}
\end{equation}
formed by the spatial \emph{shape functions} 
$\varphiz{i}$ % \in C^0_{\mathrm{c}}\left[(\zb, \zi)\right]$
and the time dependent \emph{weights} $\wi$
%where $C^p_{\mathrm{c}}\left[\Omega\right]$ is the space
%of $p$-times continously differentiable functions with 
%compact support on $\Omega$
.

Note that this separation ansatz leaves no option for the shape functions
$\varphiz{i}$ to depend on the time $t$.  Since the growing crystal and the
decrease of melt will alter the spatial domain for both phases over time it is
required to introduce a coordinate transformation in such a way that the
spatial domain in the transformed system remains constant over the process.
This can be realized by writing the system in new coordinates using a slightly
modified version of the boundary immobilisation method as used in
\citep{book:AdaptiveMethodofLines, Maidi2014}:
\begin{equation}
	\tfixz \coloneqq \torigz 
	\quad \bar z \coloneqq \tfrac{z - \zb}{\zbb} 
	\quad \zbb \coloneqq \zi - \zb,
	\label{eq:fix_trafo}
\end{equation}
whose spatial domain $\bar z \in (0,1)$ is shown on the right-hand side of
Figure \ref{fig:system}.
Note that this mapping is only well-defined as long as
there is a crystal and a melt, thus $\zi \neq \zb$.
However, since the process is started with a seed crystal this does not
pose a problem in the beginning and once the melt is solidified the problem
reduces to a single heat equation with constant domain, rendering it even
simpler in the end.

Thus, the two resulting fixed systems coupled via $\vi$ which is in turn given by the
transformed variant of the Stefan condition \eqref{eq:stefan_cond} are obtained
after short computation\footnote{%
	Please refer to Appendix \ref{app:trafo_bim} for details.
}:
\begin{subequations}
	\begin{align}
		\label{eq:fixiated_heat_eq_pde}
		\begin{split}
			\partial_t \tfixz &= a_2(t)\partial_{\bar z}^2\tfixz
			+ a_1(\bar z, t) \partial_{\bar z} \tfixz
			\\&\quad
			+\psibv\inpbmv
		\end{split}
		\\
		%\partial_t \tfixz &= \frac{\hd}{\zbbsq}\partial_{\bar z}^2\tfixz
		%+ \frac{\bar z \vi}{\zbb} \partial_{\bar z} \tfixz 
		%\nonumber\\& \hphantom{=}
		%-\frac{v_z(t)}{\zbb} \partial_{\bar z} \tfixz
		%+\frac{2}{R \stc \dn}\bar{q}_M(\bar{z},t)\\
		\label{eq:fixiated_heat_eq_bc1}
		\partial_{\bar z} \tfix(0, t) &= \inpb \\
		%\hc\partial_{\bar z} \tfix(0, t) &= \bfd\zbb\inp \\
		\label{eq:fixiated_heat_eq_bc2}
		\tfix(1, t) &= \tm \\
		\label{eq:fixiated_stefan_cond}
		\vi &= \scs\partial_{\bar z}\tfixs(1, t) 
			+ \scl\partial_{\bar z}\tfixl(1, t)
		%\vi &= \frac{1}{L\dmelt} \Bigg[
			%\frac{\hcs}{\zbbs}\partial_{\bar z}\tfixs(1, t)
			%-\frac{\hcl}{\zbbl}\tfixl(1, t)
		%\Bigg] 
			.
\end{align}
	\label{eq:fixiated_heat_eq}%
\end{subequations}%
Amongst other things, the substitutions $a_2(t) \coloneqq \hd/\zbbsq$ and
$a_1(\bar z, t) \coloneqq \left(\bar z\vi - \bar\psi_{\mathrm{v}}(\bar z)\inpbc\right)/\zbb$
%$\qmb \coloneqq \qm$, $\inpb \coloneqq \frac{\bfd}{\hc}\zbb\inp$, and 
%$\sct \coloneqq -\bfd\hc/(L\dmelt\zbb)$ 
were made.

Now, the approximation of the series expansion \eqref{eq:gf_series_trunc} can
be expressed in the transformed coordinates
\begin{equation}
	\tappz = \sum\limits_{i=1}^N \varphibz{i}\wbi{i}
	\label{eq:gf_series_fixed}
\end{equation} %jcw: nachfolgenden Absatz ggf. noch präzisieren
with the shape functions $\varphibz{i}$, % \in C^0_{\mathrm{c}}(0, 1)$,
$i=1,\dotsc,N$.
Note that herein only one set of shape functions
%$\varphibz{i}, i=1,\dotsc,N$
is utilised for both phases due to their consistent domains.
For the approximation, piecewise-linear Lagrangian (aka ``hat'') functions 
with the spatial discretisation $\dzb \coloneqq \frac{1}{N-1}$, are used.
In detail, they are given by
\begin{subequations}
	\begin{align}
		\varphibz{i} &\coloneqq \begin{cases}
			\frac{\bar z - (i - 2)\dzb}{\dzb} 
				&\!\!\!\text{for } (i - 2)\dzb < \bar z \le (i-1)\dzb \\[.5em]
			\frac{i\dzb - \bar z}{\dzb} 
				&\!\!\!\text{for } (i-1)\dzb < \bar z < i\dzb\\[.5em]
			0 
				&\!\!\!\text{otherwise}
		\end{cases}
		\label{eq:lag1st_center}
		\intertext{for the general case $i \not\in \left\{1, N\right\}$ and}
		\varphibz{1} &\coloneqq \begin{cases}
			\frac{\dzb - \bar z}{\dzb} &\text{for } 0 < \bar z < \dzb\\
			0 & \text{otherwise}
		\end{cases}
		\label{eq:lag1st_left}
		\intertext{as well as}
		\varphibz{N} &\coloneqq \begin{cases}
			\frac{\bar z - (N - 2)\dzb}{\dzb} &\text{for } (N - 2)\dzb < \bar z \le 1\\
			0 & \text{otherwise}
		\end{cases}
		\label{eq:lag1st_right}
	\end{align}%
	\label{eq:lag1st}%
\end{subequations}%
at the outer and inner boundary, respectively.

Hence, the Dirichlet boundary condition \eqref{eq:fixiated_heat_eq_bc2}
directly evaluates to $\wbi{N} = \tm$, since only $\varphibz{N}$
differs from zero at $\bar z = 1$.
Thus, by introducing the vector of the remaining free variables 
$\wbv^T \coloneqq \begin{pmatrix}
	\wbi{1}	&\cdots &\wbi{N-1}
\end{pmatrix}$
and the shape function vector
$\varphibvz \coloneqq \begin{pmatrix} 
	\varphibz{1} &\cdots &\varphibz{N-1}
\end{pmatrix}$, the approximation \eqref{eq:gf_series_fixed} can be given by 
\begin{equation}
	\tappz = \varphibvz \wbv + \varphibz{N}\tm.
	\label{eq:fix_approx}
\end{equation}

\subsection{Projection}
Using the Galerkin method, \eqref{eq:fixiated_heat_eq_pde} is projected on the
functions from $\varphibvz$, yielding equations of the form
\begin{align}
	\begin{split}
		\inner{\partial_t \tappz}{\varphibz{k}}
		&= a_2(t)\inner{\partial_{\bar z}^2\tappz}{\varphibz{k}}
		\\&\hphantom{=}
		+\inner{a_1(\bar z, t) \partial_{\bar z} \tappz}{\varphibz{k}}
		\\&\hphantom{=}
		+ \inner{\psibv}{\varphibz{k}}\inpbmv
	\end{split}
	\label{eq:fixiated_heat_eq_weak_1}
\end{align}
for $k = 1,\dotsc,N-1$.
Herein, $\inner{a(\xi)}{b(\xi)} = \int_0^1 a(\xi)b(\xi)\,\mathrm{d}\xi$
denotes the standard inner product on $L_2$ for real-valued arguments.
Performing integration by parts on the second order term of
\eqref{eq:fixiated_heat_eq_weak_1} gives the weak form
of the problem:
\begin{align}
	\begin{split}
		%\MoveEqLeft[2.1]
		\inner{\partial_t \tappz}{\varphibz{k}}
		&= 
		a_2(t) \Big(
			\left[\partial_{\bar z}\tappz \varphibz{k}\right]_0^1
			%\hfill
			\\ 
			&\qquad	-\inner{\partial_{\bar z}\tappz}{\partial_{\bar z}\varphibz{k}}
		\Big)
		\\ 
		&\mkern-140mu
		+\! \inner{a_1(\bar z, t) \partial_{\bar z} \tappz}{\varphibz{k}}
		\!+\! \inner{\psibv}{\varphibz{k}}\inpbmv.
	\end{split}
	\label{eq:fixiated_heat_eq_weak_2}
\end{align}
Finally, by utilising the boundary conditions \eqref{eq:fixiated_heat_eq_bc1},
\eqref{eq:fixiated_heat_eq_bc2} and substituting the test solution
\eqref{eq:fix_approx} the system can be written in matrix-vector notation:
\begin{align}
	\begin{split}
		\wbvt =&
		-a_2(t)\Big(
			\bs{P}_{2}\wbv
			+ \bs{q}_{2}\tm
			+ \bs{q}_{0}\inpb
		\Big)\\
		&+ \tfrac{\vi}{\beta(t)}\left(
			\bs{P}_{1} \wbv
			+ \bs{q}_{1}\tm
		\right)\\
		&- \tfrac{\inpbc}{\beta(t)}\left(
			\bs{P}_{3} \wbv
			+ \bs{q}_{3}\tm
		\right)
		%\\ &
		+ \bs{\Xi}\bs{\Theta}(t)\inpbmv
	\end{split}
	\label{eq:gen_ode_sys}
\end{align}
with the matrices $\bs{P}_{i}$, $\bs{\Xi}$ and $\bs{\Theta}(t)$ as well as
the vectors $\bs{q}_{i}$ given in Appendix \ref{app:matrices}.
Thus, \glspl{odes} for the weights of the solid and liquid phase
-- given by $\wbvs$ and $\wbvl$, respectively --
are obtained.
Finally, writing \eqref{eq:fixiated_stefan_cond} in the new variables yields:
\begin{align}
	\begin{split}
		\vi &= 
		\partial_{\bar z}\bar{\bs{\varphi}}^{N}\!(1)
		\Big(
			\scs \wbvs + \scl \wbvl
		\Big)
		\\&\quad +
		\partial_{\bar z}\varphib{N}(1) \tm 
		\Big(
			\scs + \scl
		\Big).
	\end{split}
	\label{eq:fixiated_stefan_cond_approx}
\end{align}%


\subsection{State space formulation}

In order to make the derived system compatible with the general state space
formulation \eqref{eq:nonlin_ss}, firstly, the state vector
\begin{equation}
	\bs{x}^N\!(t) \coloneqq \begin{pmatrix}
		\left(\wbvs\right)^T 
		&\left(\wbvl\right)^T 
		&\zi
	\end{pmatrix}^T \in \mathbb{R}^M 
	\label{eq:state_vector}
\end{equation}
with dimension $M \coloneqq 2(N-1) + 1$, containing the temperature weights of
crystal and melt as well as the interface position, is introduced.
Regarding the initial state $\bs{x}^N\!(0)$, the initial weights for the solid or
liquid phase $\bar{w}_{i}^N\!(0)$ are obtained by the projection
\begin{equation}
	\wb_{i}^N\!(0) = \inner{\bar\varphi^N_i\!(\bar z)}{\tfix(\bar z, 0)},
		\quad i=1,\dotsc,N-1
	\label{eq:initial_weights}
\end{equation}
of the respective initial temperature profile onto the shape functions
$\varphibz{i}$. 
Finally, the system input is given by
\begin{equation}
	\uo \coloneqq \begin{pmatrix}
		\inpbs &\inpbm{1} &\inpbm{2} &\inpbm{3} &\inpbl &\inpbc
	\end{pmatrix}^T
	\label{eq:input_vector}
\end{equation}
including the top, jacket and bottom heaters as well as the melt convection.
Thus, by using the solid and liquid variant of \eqref{eq:gen_ode_sys} as well
as \eqref{eq:fixiated_stefan_cond_approx}, the approximated form reads:
\begin{equation}
	\dot{\bs{x}}^N\!(t) = f^N\!\left(\bs{x}^N\!(t), \uo\right)
	%= \begin{pmatrix}
		%\left(\wbvts\right)^T 
		%&\left(\wbvtl\right)^T 
		%&\vi
	%\end{pmatrix}
	.
	\label{eq:approx_dynamics}
\end{equation}
Since only approximations of the same order will be used in the
following sections, the order $N$ will be omitted where the context
is clear.


%%% Local Variables:
%%% mode: latex
%%% TeX-master: "article"
%%% End:
