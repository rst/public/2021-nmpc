# Model Predictive Control of the Vertical Gradient Freeze Crystal Growth Process #

## About ##
This repository serves to provide the reader of the contribution *Model Predictive Control of the Vertical Gradient Freeze Crystal Growth Process*
by Stefan Ecklebe, Tom Buchwald, Patrick Rüdiger, and Jan Winkler submitted to the
[7th IFAC Conference on Nonlinear Model Predictive Control 2021](https://www.nmpc2021.org/)
with all relevant source code. The document and all results presented therein may be reproduced following the instructions given below.


## License ##
All files in the `python` folder are licensed under the terms of the 
GNU General Public License v3.0.
As for the manuscript, the authors grant IFAC the right to publish the content
from the `include` and `latex` folders in terms of the Creative Commons 
CC-BY-NC-ND license.

## Structure of this repository ##
- `python`: **Code that runs all simulations**
- `include`: Graphics for the manuscript
- `manuscript`: LaTeX sources for the manuscript
- `talk`: LaTeX sources for the talk
- `docs`: Form and style guides
- `submission`: Files regarding the submission process

## Installation ##
The contents of this repository may be cloned to your system running

	$ git clone

All simulations can be found in the `python` folder relying on the
submodule `rst-mpctools` which can be obtained running

	$ git submodule init
	$ git submodule update

Please refer to the submodule's readme for further instructions.

## Usage ##

To run the simulations discussed in the paper use the following scripts.
The results are stored as pickle files in the data folder.
	
### Preliminaries ###

The Verification of the VGF Model (see Appendix D) is done by:

1. `stefan/test_analytic.py`:
Calculates the temperature distribution and phase boundary position over time according to the neumann solution. 

and 

2. `stefan/test_model.py`:
calculates the deviation of the position of the phase boundary over time computed by the neumann solution and fe models using different approximation orders. (Fig. 3)

### MPC Simulation case A, B, C ###

Run `control.py` to compute the MPC simulation data discussed in section 5.3 and 5.4.


### Plots ###

To generate the result plots, run `create_plots.py` after all other scripts were run successfully.


## Feedback ##

As we are involved in control engineering, feedback is very important for us.
If you happen to find any errors or got any questions please feel free to
open an issue or contact `stefan.ecklebe@tu-dresden.de`.


